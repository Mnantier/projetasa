/**
 */
package asa;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interface Connecteur</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link asa.InterfaceConnecteur#getRole <em>Role</em>}</li>
 * </ul>
 *
 * @see asa.AsaPackage#getInterfaceConnecteur()
 * @model
 * @generated
 */
public interface InterfaceConnecteur extends Interface {
	/**
	 * Returns the value of the '<em><b>Role</b></em>' containment reference list.
	 * The list contents are of type {@link asa.Role}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role</em>' containment reference list.
	 * @see asa.AsaPackage#getInterfaceConnecteur_Role()
	 * @model containment="true"
	 * @generated
	 */
	EList<Role> getRole();

} // InterfaceConnecteur
