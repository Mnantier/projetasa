/**
 */
package asa;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Service</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link asa.Service#getNom <em>Nom</em>}</li>
 *   <li>{@link asa.Service#getService <em>Service</em>}</li>
 *   <li>{@link asa.Service#getServiceeOpposite <em>Servicee Opposite</em>}</li>
 * </ul>
 *
 * @see asa.AsaPackage#getService()
 * @model
 * @generated
 */
public interface Service extends EObject {
	/**
	 * Returns the value of the '<em><b>Nom</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nom</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nom</em>' attribute.
	 * @see #setNom(String)
	 * @see asa.AsaPackage#getService_Nom()
	 * @model
	 * @generated
	 */
	String getNom();

	/**
	 * Sets the value of the '{@link asa.Service#getNom <em>Nom</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nom</em>' attribute.
	 * @see #getNom()
	 * @generated
	 */
	void setNom(String value);

	/**
	 * Returns the value of the '<em><b>Service</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link asa.Service#getServiceeOpposite <em>Servicee Opposite</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Service</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Service</em>' reference.
	 * @see #setService(Service)
	 * @see asa.AsaPackage#getService_Service()
	 * @see asa.Service#getServiceeOpposite
	 * @model opposite="serviceeOpposite" required="true"
	 * @generated
	 */
	Service getService();

	/**
	 * Sets the value of the '{@link asa.Service#getService <em>Service</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Service</em>' reference.
	 * @see #getService()
	 * @generated
	 */
	void setService(Service value);

	/**
	 * Returns the value of the '<em><b>Servicee Opposite</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link asa.Service#getService <em>Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Servicee Opposite</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Servicee Opposite</em>' reference.
	 * @see #setServiceeOpposite(Service)
	 * @see asa.AsaPackage#getService_ServiceeOpposite()
	 * @see asa.Service#getService
	 * @model opposite="service"
	 * @generated
	 */
	Service getServiceeOpposite();

	/**
	 * Sets the value of the '{@link asa.Service#getServiceeOpposite <em>Servicee Opposite</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Servicee Opposite</em>' reference.
	 * @see #getServiceeOpposite()
	 * @generated
	 */
	void setServiceeOpposite(Service value);

} // Service
