/**
 */
package asa.impl;

import asa.AsaFactory;
import asa.AsaPackage;
import asa.Composant;
import asa.Connecteur;
import asa.InterfaceComposant;
import asa.InterfaceConnecteur;
import asa.Port;
import asa.Role;
import asa.Service;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AsaFactoryImpl extends EFactoryImpl implements AsaFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AsaFactory init() {
		try {
			AsaFactory theAsaFactory = (AsaFactory) EPackage.Registry.INSTANCE.getEFactory(AsaPackage.eNS_URI);
			if (theAsaFactory != null) {
				return theAsaFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new AsaFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AsaFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case AsaPackage.SYSTEM:
			return createSystem();
		case AsaPackage.COMPOSANT:
			return createComposant();
		case AsaPackage.CONNECTEUR:
			return createConnecteur();
		case AsaPackage.INTERFACE_COMPOSANT:
			return createInterfaceComposant();
		case AsaPackage.INTERFACE_CONNECTEUR:
			return createInterfaceConnecteur();
		case AsaPackage.PORT:
			return createPort();
		case AsaPackage.ROLE:
			return createRole();
		case AsaPackage.SERVICE:
			return createService();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public asa.System createSystem() {
		SystemImpl system = new SystemImpl();
		return system;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Composant createComposant() {
		ComposantImpl composant = new ComposantImpl();
		return composant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Connecteur createConnecteur() {
		ConnecteurImpl connecteur = new ConnecteurImpl();
		return connecteur;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public InterfaceComposant createInterfaceComposant() {
		InterfaceComposantImpl interfaceComposant = new InterfaceComposantImpl();
		return interfaceComposant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public InterfaceConnecteur createInterfaceConnecteur() {
		InterfaceConnecteurImpl interfaceConnecteur = new InterfaceConnecteurImpl();
		return interfaceConnecteur;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Port createPort() {
		PortImpl port = new PortImpl();
		return port;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Role createRole() {
		RoleImpl role = new RoleImpl();
		return role;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Service createService() {
		ServiceImpl service = new ServiceImpl();
		return service;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AsaPackage getAsaPackage() {
		return (AsaPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static AsaPackage getPackage() {
		return AsaPackage.eINSTANCE;
	}

} //AsaFactoryImpl
