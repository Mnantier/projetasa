/**
 */
package asa.impl;

import asa.AsaPackage;
import asa.Service;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Service</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link asa.impl.ServiceImpl#getNom <em>Nom</em>}</li>
 *   <li>{@link asa.impl.ServiceImpl#getService <em>Service</em>}</li>
 *   <li>{@link asa.impl.ServiceImpl#getServiceeOpposite <em>Servicee Opposite</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ServiceImpl extends MinimalEObjectImpl.Container implements Service {
	/**
	 * The default value of the '{@link #getNom() <em>Nom</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNom()
	 * @generated
	 * @ordered
	 */
	protected static final String NOM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNom() <em>Nom</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNom()
	 * @generated
	 * @ordered
	 */
	protected String nom = NOM_EDEFAULT;

	/**
	 * The cached value of the '{@link #getService() <em>Service</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getService()
	 * @generated
	 * @ordered
	 */
	protected Service service;

	/**
	 * The cached value of the '{@link #getServiceeOpposite() <em>Servicee Opposite</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServiceeOpposite()
	 * @generated
	 * @ordered
	 */
	protected Service serviceeOpposite;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ServiceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AsaPackage.Literals.SERVICE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getNom() {
		return nom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNom(String newNom) {
		String oldNom = nom;
		nom = newNom;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AsaPackage.SERVICE__NOM, oldNom, nom));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Service getService() {
		if (service != null && service.eIsProxy()) {
			InternalEObject oldService = (InternalEObject) service;
			service = (Service) eResolveProxy(oldService);
			if (service != oldService) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AsaPackage.SERVICE__SERVICE, oldService,
							service));
			}
		}
		return service;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Service basicGetService() {
		return service;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetService(Service newService, NotificationChain msgs) {
		Service oldService = service;
		service = newService;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AsaPackage.SERVICE__SERVICE,
					oldService, newService);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setService(Service newService) {
		if (newService != service) {
			NotificationChain msgs = null;
			if (service != null)
				msgs = ((InternalEObject) service).eInverseRemove(this, AsaPackage.SERVICE__SERVICEE_OPPOSITE,
						Service.class, msgs);
			if (newService != null)
				msgs = ((InternalEObject) newService).eInverseAdd(this, AsaPackage.SERVICE__SERVICEE_OPPOSITE,
						Service.class, msgs);
			msgs = basicSetService(newService, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AsaPackage.SERVICE__SERVICE, newService, newService));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Service getServiceeOpposite() {
		if (serviceeOpposite != null && serviceeOpposite.eIsProxy()) {
			InternalEObject oldServiceeOpposite = (InternalEObject) serviceeOpposite;
			serviceeOpposite = (Service) eResolveProxy(oldServiceeOpposite);
			if (serviceeOpposite != oldServiceeOpposite) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AsaPackage.SERVICE__SERVICEE_OPPOSITE,
							oldServiceeOpposite, serviceeOpposite));
			}
		}
		return serviceeOpposite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Service basicGetServiceeOpposite() {
		return serviceeOpposite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetServiceeOpposite(Service newServiceeOpposite, NotificationChain msgs) {
		Service oldServiceeOpposite = serviceeOpposite;
		serviceeOpposite = newServiceeOpposite;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					AsaPackage.SERVICE__SERVICEE_OPPOSITE, oldServiceeOpposite, newServiceeOpposite);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setServiceeOpposite(Service newServiceeOpposite) {
		if (newServiceeOpposite != serviceeOpposite) {
			NotificationChain msgs = null;
			if (serviceeOpposite != null)
				msgs = ((InternalEObject) serviceeOpposite).eInverseRemove(this, AsaPackage.SERVICE__SERVICE,
						Service.class, msgs);
			if (newServiceeOpposite != null)
				msgs = ((InternalEObject) newServiceeOpposite).eInverseAdd(this, AsaPackage.SERVICE__SERVICE,
						Service.class, msgs);
			msgs = basicSetServiceeOpposite(newServiceeOpposite, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AsaPackage.SERVICE__SERVICEE_OPPOSITE,
					newServiceeOpposite, newServiceeOpposite));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AsaPackage.SERVICE__SERVICE:
			if (service != null)
				msgs = ((InternalEObject) service).eInverseRemove(this, AsaPackage.SERVICE__SERVICEE_OPPOSITE,
						Service.class, msgs);
			return basicSetService((Service) otherEnd, msgs);
		case AsaPackage.SERVICE__SERVICEE_OPPOSITE:
			if (serviceeOpposite != null)
				msgs = ((InternalEObject) serviceeOpposite).eInverseRemove(this, AsaPackage.SERVICE__SERVICE,
						Service.class, msgs);
			return basicSetServiceeOpposite((Service) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AsaPackage.SERVICE__SERVICE:
			return basicSetService(null, msgs);
		case AsaPackage.SERVICE__SERVICEE_OPPOSITE:
			return basicSetServiceeOpposite(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AsaPackage.SERVICE__NOM:
			return getNom();
		case AsaPackage.SERVICE__SERVICE:
			if (resolve)
				return getService();
			return basicGetService();
		case AsaPackage.SERVICE__SERVICEE_OPPOSITE:
			if (resolve)
				return getServiceeOpposite();
			return basicGetServiceeOpposite();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AsaPackage.SERVICE__NOM:
			setNom((String) newValue);
			return;
		case AsaPackage.SERVICE__SERVICE:
			setService((Service) newValue);
			return;
		case AsaPackage.SERVICE__SERVICEE_OPPOSITE:
			setServiceeOpposite((Service) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AsaPackage.SERVICE__NOM:
			setNom(NOM_EDEFAULT);
			return;
		case AsaPackage.SERVICE__SERVICE:
			setService((Service) null);
			return;
		case AsaPackage.SERVICE__SERVICEE_OPPOSITE:
			setServiceeOpposite((Service) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AsaPackage.SERVICE__NOM:
			return NOM_EDEFAULT == null ? nom != null : !NOM_EDEFAULT.equals(nom);
		case AsaPackage.SERVICE__SERVICE:
			return service != null;
		case AsaPackage.SERVICE__SERVICEE_OPPOSITE:
			return serviceeOpposite != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (nom: ");
		result.append(nom);
		result.append(')');
		return result.toString();
	}

} //ServiceImpl
