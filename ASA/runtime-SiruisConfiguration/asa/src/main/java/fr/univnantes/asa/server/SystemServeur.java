package fr.univnantes.asa.server;


public class SystemServeur {

    private  ConnexionManager ConnexionManager;
    private  Database Database;
    private  SecurityManager SecurityManager;
	
	public SystemServeur(){}

	public SystemServeur(ConnexionManager connexionManager,
			Database database, SecurityManager securityManager) {
		super();
		ConnexionManager = connexionManager;
		Database = database;
		SecurityManager = securityManager;
	}



	public ConnexionManager getConnexionManager() {
		return ConnexionManager;
	}

	public void setConnexionManager(ConnexionManager connexionManager) {
		ConnexionManager = connexionManager;
	}

	public Database getDatabase() {
		return Database;
	}

	public void setDatabase(Database database) {
		Database = database;
	}

	public SecurityManager getSecurityManager() {
		return SecurityManager;
	}

	public void setSecurityManager(SecurityManager securityManager) {
		SecurityManager = securityManager;
	}


}
