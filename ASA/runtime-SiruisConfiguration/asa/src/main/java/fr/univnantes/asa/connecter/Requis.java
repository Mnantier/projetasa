package fr.univnantes.asa.connecter;
import javax.management.relation.Role;

public class Requis{
	
   	private boolean requis;
   	private String nom;
   	private RoleRequis RPCRequis;

	public void Requis(){}

	public Requis(boolean requis, String nom, RoleRequis rPCRequis) {
		super();
		this.requis = requis;
		this.nom = nom;
		RPCRequis = rPCRequis;
	}

	public boolean isRequis() {
		return requis;
	}

	public void setRequis(boolean requis) {
		this.requis = requis;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public RoleRequis getRPCRequis() {
		return RPCRequis;
	}

	public void setRPCRequis(RoleRequis rPCRequis) {
		RPCRequis = rPCRequis;
	}


}	
