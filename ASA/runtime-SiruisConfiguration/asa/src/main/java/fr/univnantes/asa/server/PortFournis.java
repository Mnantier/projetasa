package fr.univnantes.asa.server;

public class PortFournis {
private String msg;
	
	public synchronized void Send(String msg) {
		this.msg = msg;
		notify();
	}
	
	public synchronized String getMsg() {
		while(msg == null)
		{	try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String m = msg;
		msg = null;
		notify();
		return m;
	}
}
