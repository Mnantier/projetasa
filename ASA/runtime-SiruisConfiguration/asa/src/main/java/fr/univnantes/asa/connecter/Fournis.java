package fr.univnantes.asa.connecter;

public class Fournis{
	
   	private boolean requis;
   	private String nom;
   	private RoleFournis RPCFournis;

   	public Fournis(RoleFournis RPCFournis){
		this.RPCFournis = RPCFournis;
	}


	public Fournis(boolean requis, String nom, RoleFournis rPCFournis) {
		super();
		this.requis = requis;
		this.nom = nom;
		RPCFournis = rPCFournis;
	}


	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Role getRPCFournis() {
		return RPCFournis;
	}

	public void setRPCFournis(Role rPCFournis) {
		RPCFournis = (RoleFournis) rPCFournis;
	}

	public boolean isRequis() {
		return requis;
	}

	public void setRequis(boolean requis) {
		this.requis = requis;
	}

  
}	
