package fr.univnantes.asa.connecter;

public interface Role {

	public void Send(String msg);
	
	public String getMsg();
}
