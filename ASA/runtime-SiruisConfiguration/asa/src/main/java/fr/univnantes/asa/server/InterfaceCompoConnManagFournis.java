package fr.univnantes.asa.server;
import javax.sound.sampled.Port;

public class InterfaceCompoConnManagFournis{
	
   	private boolean requis;
   	private String nom;
   	private PortFournis ExternalSocket;

	public InterfaceCompoConnManagFournis(boolean requis, String nom, PortFournis externalSocket) {
		super();
		this.requis = requis;
		this.nom = nom;
		ExternalSocket = externalSocket;
	}

	public void InterfaceCompoConnManagFournis(){}

	public boolean isRequis() {
		return requis;
	}

	public void setRequis(boolean requis) {
		this.requis = requis;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public PortFournis getExternalSocket() {
		return ExternalSocket;
	}

	public void setExternalSocket(PortFournis externalSocket) {
		ExternalSocket = externalSocket;
	}


}	
