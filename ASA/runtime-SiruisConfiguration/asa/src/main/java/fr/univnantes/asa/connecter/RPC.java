package fr.univnantes.asa.connecter;
public class RPC {
	
   	private Requis Requis;
   	private Fournis Fournis;
	private String nom;

	public RPC(RoleFournis RPCFournis){
		this.Fournis = new Fournis(RPCFournis);
	}

	public RPC(fr.univnantes.asa.connecter.Requis requis, fr.univnantes.asa.connecter.Fournis fournis, String nom) {
		super();
		Requis = requis;
		Fournis = fournis;
		this.nom = nom;
	}

	public Requis getRequis() {
		return Requis;
	}

	public void setRequis(Requis requis) {
		Requis = requis;
	}

	public Fournis getFournis() {
		return Fournis;
	}

	public void setFournis(Fournis fournis) {
		Fournis = fournis;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

   
}	
