package fr.univnantes.asa.server;

public class ConnexionManager {
	
   	private InterfaceCompoConnManagFournis InterfaceCompoConnManagFournis;
   	private InterfaceCompoConnManagRequis InterfaceCompoConnManagRequis;


	public ConnexionManager(fr.univnantes.asa.server.InterfaceCompoConnManagFournis interfaceCompoConnManagFournis,
			fr.univnantes.asa.server.InterfaceCompoConnManagRequis interfaceCompoConnManagRequis) {
		super();
		InterfaceCompoConnManagFournis = interfaceCompoConnManagFournis;
		InterfaceCompoConnManagRequis = interfaceCompoConnManagRequis;
	}


	public void ConnexionManager(){}


	public InterfaceCompoConnManagFournis getInterfaceCompoConnManagFournis() {
		return InterfaceCompoConnManagFournis;
	}


	public void setInterfaceCompoConnManagFournis(InterfaceCompoConnManagFournis interfaceCompoConnManagFournis) {
		InterfaceCompoConnManagFournis = interfaceCompoConnManagFournis;
	}


	public InterfaceCompoConnManagRequis getInterfaceCompoConnManagRequis() {
		return InterfaceCompoConnManagRequis;
	}


	public void setInterfaceCompoConnManagRequis(InterfaceCompoConnManagRequis interfaceCompoConnManagRequis) {
		InterfaceCompoConnManagRequis = interfaceCompoConnManagRequis;
	}




}	
