package fr.univnantes.asa.connecter;
import javax.sound.sampled.Port;

public class RPCRequis {
	
   	private Port ExternalSocket;

	public RPCRequis(Port externalSocket) {
		super();
		ExternalSocket = externalSocket;
	}

	public void ExternalSocket(){}

	public Port getExternalSocket() {
		return ExternalSocket;
	}

	public void setExternalSocket(Port externalSocket) {
		ExternalSocket = externalSocket;
	}

 
}	
