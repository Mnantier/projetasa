package fr.univnantes.asa.client;
import javax.sound.sampled.Port;

import fr.univnantes.asa.connecter.Role;
import fr.univnantes.asa.connecter.RoleFournis;

public class PortClientRequis {
	
   	private Role roleFournis;

	public PortClientRequis(){
		roleFournis = new RoleFournis();
	}

	public PortClientRequis(Role roleFournis) {
		super();
		this.roleFournis = roleFournis;
	}

	public Role getRoleFournis() {
		return roleFournis;
	}

	public void setRoleFournis(Role roleFournis) {
		this.roleFournis = roleFournis;
	}


 
}	
