package fr.univnantes.asa.client;

public class Client {
	
   	private InterfaceRequisCompoClient InterfaceRequisCompoClient;


	public Client(){
		InterfaceRequisCompoClient = new InterfaceRequisCompoClient();
	}

    public Client(fr.univnantes.asa.client.InterfaceRequisCompoClient interfaceRequisCompoClient) {
		super();
		InterfaceRequisCompoClient = interfaceRequisCompoClient;
	}

	public void setInterfaceRequisCompoClient(InterfaceRequisCompoClient InterfaceRequisCompoClient) {
        this.InterfaceRequisCompoClient = InterfaceRequisCompoClient;
    }

    public InterfaceRequisCompoClient getInterfaceRequisCompoClient() {
        return this.InterfaceRequisCompoClient;
    }


}	
