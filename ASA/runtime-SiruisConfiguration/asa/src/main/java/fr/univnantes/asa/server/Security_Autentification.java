package fr.univnantes.asa.server;
public class Security_Autentification {
	
	private String msg;
	private String reponse;
	
	public synchronized String check(String msg) {
		this.msg = msg;
		notify();
		while(reponse == null)
		{	try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String m = reponse;
		reponse = null;
		notify();
		return m;
	}
	
	public synchronized String getMsg() {
		while(msg == null)
		{	try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String m = msg;
		msg = null;
		notify();
		return m;
	}
	
	public synchronized void setReposnse(String rep) {
		while(reponse != null)
		{	try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		reponse = rep;
		notify();
	}


}	
