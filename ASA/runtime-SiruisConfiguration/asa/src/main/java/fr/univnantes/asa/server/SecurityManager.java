package fr.univnantes.asa.server;
public class SecurityManager {
	
   	private InterfaceCompoSecuManagFournis InterfaceCompoSecuManagFournis;
   	private InterfaceCompoSecuManagRequis InterfaceCompoSecuManagRequis;


	public void SecurityManager(){}


	public InterfaceCompoSecuManagFournis getInterfaceCompoSecuManagFournis() {
		return InterfaceCompoSecuManagFournis;
	}


	public void setInterfaceCompoSecuManagFournis(InterfaceCompoSecuManagFournis interfaceCompoSecuManagFournis) {
		InterfaceCompoSecuManagFournis = interfaceCompoSecuManagFournis;
	}


	public InterfaceCompoSecuManagRequis getInterfaceCompoSecuManagRequis() {
		return InterfaceCompoSecuManagRequis;
	}


	public void setInterfaceCompoSecuManagRequis(InterfaceCompoSecuManagRequis interfaceCompoSecuManagRequis) {
		InterfaceCompoSecuManagRequis = interfaceCompoSecuManagRequis;
	}


	public SecurityManager(fr.univnantes.asa.server.InterfaceCompoSecuManagFournis interfaceCompoSecuManagFournis,
			fr.univnantes.asa.server.InterfaceCompoSecuManagRequis interfaceCompoSecuManagRequis) {
		super();
		InterfaceCompoSecuManagFournis = interfaceCompoSecuManagFournis;
		InterfaceCompoSecuManagRequis = interfaceCompoSecuManagRequis;
	}




}	
