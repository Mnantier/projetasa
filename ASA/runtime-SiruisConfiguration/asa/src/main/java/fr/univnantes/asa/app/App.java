package fr.univnantes.asa.app;



import fr.univnantes.asa.SystemComplet;
import fr.univnantes.asa.client.Client;
import fr.univnantes.asa.client.InterfaceRequisCompoClient;
import fr.univnantes.asa.client.PortClientRequis;
import fr.univnantes.asa.connecter.Fournis;
import fr.univnantes.asa.connecter.RPC;
import fr.univnantes.asa.connecter.Requis;
import fr.univnantes.asa.connecter.RoleFournis;
import fr.univnantes.asa.connecter.RoleRequis;
import fr.univnantes.asa.server.Check_query;
import fr.univnantes.asa.server.ConnexionManager;
import fr.univnantes.asa.server.DB_query;
import fr.univnantes.asa.server.Database;
import fr.univnantes.asa.server.InterfaceCompoConnManagFournis;
import fr.univnantes.asa.server.InterfaceCompoConnManagRequis;
import fr.univnantes.asa.server.InterfaceCompoDataFournis;
import fr.univnantes.asa.server.InterfaceCompoSecuManagFournis;
import fr.univnantes.asa.server.InterfaceCompoSecuManagRequis;
import fr.univnantes.asa.server.PortFournis;
import fr.univnantes.asa.server.Query_Interrogation;
import fr.univnantes.asa.server.SecurityCheck;
import fr.univnantes.asa.server.SecurityManagement;
import fr.univnantes.asa.server.SecurityManager;
import fr.univnantes.asa.server.Security_Autentification;
import fr.univnantes.asa.server.Serveur;
import fr.univnantes.asa.server.SystemServeur;

public class App {


	public static void main(String args[]) {
		
		SystemComplet sys = initSystem();

		Thread clientThread = new Thread(new ClientRun(sys.getClient()));
		clientThread.start();

		Thread connecteurThread = new Thread(new ConnecterRun(sys.getRPC()));
		connecteurThread.start();
		
		Thread serverThread = new Thread(new ServerRun(sys.getServeur()));
		serverThread.start();
	}


	private static SystemComplet initSystem() {
		Database  database = new Database(new InterfaceCompoDataFournis(false, "nom", new Query_Interrogation(), new SecurityManagement()));
		fr.univnantes.asa.server.SecurityManager securityManager = new SecurityManager(new InterfaceCompoSecuManagFournis(false, "nom", new Security_Autentification()), new InterfaceCompoSecuManagRequis(true, "nom", new Check_query(database.getInterfaceCompoDataFournis().getSecurityManagement())));
		SecurityCheck securityCheck = new SecurityCheck(securityManager.getInterfaceCompoSecuManagFournis().getSecurity_Autentification());
		DB_query dB_query = new DB_query(database.getInterfaceCompoDataFournis().getQuery_Interrogation());
		ConnexionManager connexionManager = new ConnexionManager(new InterfaceCompoConnManagFournis(false, "nom", new PortFournis()), new InterfaceCompoConnManagRequis(true, "nom", securityCheck, dB_query));
		Serveur serveur = new Serveur(new SystemServeur(connexionManager, database, securityManager));
		RPC rPC = new RPC(new Requis(true, "nom", new RoleRequis(serveur.getSystemServeur().getConnexionManager().getInterfaceCompoConnManagFournis().getExternalSocket())),
				new Fournis(false, "nom", new RoleFournis()), "nom");
		Client client = new Client(new InterfaceRequisCompoClient(true, "nom", new PortClientRequis(rPC.getFournis().getRPCFournis())));
		
		return new SystemComplet(client, serveur, rPC);
	}
}




/*
 * 
 */
class ClientRun implements Runnable{

	Client client;
	
	public ClientRun(Client c) {
		client = c;
	}
	public void run() {
		client.getInterfaceRequisCompoClient().getPortClientRequis().getRoleFournis().Send("test");
		System.out.println("message envoyer epuis client : "+"test");
	}
	
}


/*
 * 
 */
class ConnecterRun implements Runnable{

	RPC rpc;
	
	public ConnecterRun(RPC c) {
		rpc = c;
	}
	public void run() {
		String m =rpc.getFournis().getRPCFournis().getMsg();
		System.out.println("connecteur recoit : "+ m);
		
		String m2 = "identite";
		System.out.println("connecteur transform "+ m + " en " + m2);
		rpc.getRequis().getRPCRequis().getPort().Send(m2);
		System.out.println("connecteur envoie : "+ m2);
	}
	
}



/*
 * 
 */
class ServerRun implements Runnable{

	Serveur serv;
	
	public ServerRun(Serveur s) {
		serv = s;
	}
	public void run() {
		Thread connexionThread = new Thread(new ConnexionRun(serv.getSystemServeur().getConnexionManager()));
		connexionThread.start();

		Thread securityThread = new Thread(new SecurityRun(serv.getSystemServeur().getSecurityManager()));
		securityThread.start();
		
		Thread dataThread = new Thread(new DataRun(serv.getSystemServeur().getDatabase()));
		dataThread.start();
	}
	
}



/*
 * 
 */
class ConnexionRun implements Runnable{

	ConnexionManager connex;
	
	public ConnexionRun(ConnexionManager c) {
		connex = c;
	}
	public void run() {
		String m = connex.getInterfaceCompoConnManagFournis().getExternalSocket().getMsg();
		System.out.println("connexionmanager recoit : "+m);
		System.out.println("connexionmanager envoie a secu : "+m);
		//check security
		String token = connex.getInterfaceCompoConnManagRequis().getSecurityCheck().getSec().check(m);
		System.out.println("connexionmanager token recu "+token);
		if(token !="") {
			System.out.println("connexionmanager envoie a la base "+"une request");
			connex.getInterfaceCompoConnManagRequis().getDB_query().getDB_query().Send("une request");
		}
	}
	
}



/*
 * 
 */
class SecurityRun implements Runnable{

	SecurityManager sec;
	
	public SecurityRun(SecurityManager s) {
		sec = s;
	}
	public void run() {
		String m = sec.getInterfaceCompoSecuManagFournis().getSecurity_Autentification().getMsg();
		System.out.println("secu recoit "+m);
		
		//verifie avec la base si c'est bon
		System.out.println("secu envoie a la base demande : "+m);
		String repBd = sec.getInterfaceCompoSecuManagRequis().getCheck_query().getSecu().check(m);
		System.out.println("secu recoit de la base  : "+repBd);

		sec.getInterfaceCompoSecuManagFournis().getSecurity_Autentification().setReposnse(repBd);
		System.out.println("secu renvoie "+repBd);

	}
	
}



/*
 * 
 */
class DataRun implements Runnable{

	Database data;
	
	public DataRun(Database d) {
		data = d;
	}
	public void run() {
		//pour secu
		String m = data.getInterfaceCompoDataFournis().getSecurityManagement().getMsg();
		System.out.println("bd recoit " +m);
		String rep = " ";
		if("identite".equals(m)) {
			rep = "ok";
		}
		data.getInterfaceCompoDataFournis().getSecurityManagement().setReposnse(rep);
		System.out.println("bd renvoie " +rep);
		
		
		//pour connexion
		String m2 = data.getInterfaceCompoDataFournis().getQuery_Interrogation().getMsg();
		System.out.println("bd recoit de connexion " +m2);

	}
	
	
	
}