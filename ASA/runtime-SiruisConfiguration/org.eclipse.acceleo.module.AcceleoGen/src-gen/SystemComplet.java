

public class SystemComplet {

    private asa.Composant Client;
    private asa.Composant Serveur;
    private	asa.Connecteur RPC;
	private String nom;
	
	public SystemComplet(){}


    public void setClient(asa.Composant Client) {
        this.Client = Client;
    }
    public void setServeur(asa.Composant Serveur) {
        this.Serveur = Serveur;
    }

    public void setRPC(asa.Connecteur RPC) {
        this.RPC = RPC;
    }

    public Connecteur getRPC() {
        return this.RPC;
    }

    public Composant getClient() {
        return this.Client;
    }
    public Composant getServeur() {
        return this.Serveur;
    }

	public String getName(){
		return this.nom;
	}

	public void setNom(String nom){
		this.nom = nom;
	} 

}
