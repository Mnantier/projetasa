

public class SystemServeur {

    private asa.Composant ConnexionManager;
    private asa.Composant Database;
    private asa.Composant SecurityManager;
	private String nom;
	
	public SystemServeur(){}


    public void setConnexionManager(asa.Composant ConnexionManager) {
        this.ConnexionManager = ConnexionManager;
    }
    public void setDatabase(asa.Composant Database) {
        this.Database = Database;
    }
    public void setSecurityManager(asa.Composant SecurityManager) {
        this.SecurityManager = SecurityManager;
    }



    public Composant getConnexionManager() {
        return this.ConnexionManager;
    }
    public Composant getDatabase() {
        return this.Database;
    }
    public Composant getSecurityManager() {
        return this.SecurityManager;
    }

	public String getName(){
		return this.nom;
	}

	public void setNom(String nom){
		this.nom = nom;
	} 

}
