public class RPC {
	
   	private asa.InterfaceConnecteur Requis;
   	private asa.InterfaceConnecteur Fournis;
	private String nom;

	public RPC(){}

    public void setRequis(asa.InterfaceConnecteur Requis) {
        this.Requis = Requis;
    }
    public void setFournis(asa.InterfaceConnecteur Fournis) {
        this.Fournis = Fournis;
    }
    public InterfaceConnecteur getRequis() {
        return this.Requis;
    }
    public InterfaceConnecteur getFournis() {
        return this.Fournis;
    }

    public String getNom () {
        return this.nom;
    }

	    public void setNom () {
        return this.nom;
    }
}	
