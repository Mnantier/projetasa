public class ExternalSocket {
	
   	private asa.Port ExternalSocket;
	private String msg;
	public ExternalSocket(){}

    public void setExternalSocket(ExternalSocket ExternalSocket) {
        this.ExternalSocket = ExternalSocket;
    }

    public String getExternalSocket() {
        return this.ExternalSocket;
    }
	
	public synchronized void Send(String msg) {
		this.msg = msg;
		notify();
	}
	
	public synchronized String getMsg() {
		while(msg == null)
		{	try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String m = msg;
		msg = null;
		notify();
		return m;
	}


}	
