public class PortClientRequis {
	
   	private asa.Port PortClientRequis;
	private String msg;
	public PortClientRequis(){}

    public void setPortClientRequis(PortClientRequis PortClientRequis) {
        this.PortClientRequis = PortClientRequis;
    }

    public String getPortClientRequis() {
        return this.PortClientRequis;
    }
	
	public synchronized void Send(String msg) {
		this.msg = msg;
		notify();
	}
	
	public synchronized String getMsg() {
		while(msg == null)
		{	try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String m = msg;
		msg = null;
		notify();
		return m;
	}


}	
