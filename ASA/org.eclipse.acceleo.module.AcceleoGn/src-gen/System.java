public class System {

    java.lang.String nom;

    Composant composant;
    Connecteur connecteur;

    public System(){};

    public java.lang.String getNom () {
        return this.nom;
    }

    public void setNom (java.lang.String nom) {
        this.nom = nom;
    }

    public Composant getComposant () {
        return this.composant;
    }
    public Connecteur getConnecteur () {
        return this.connecteur;
    }

    public void setComposant (Composant composant) {
        this.composant = composant;
    }
    public void setConnecteur (Connecteur connecteur) {
        this.connecteur = connecteur;
    }


}
