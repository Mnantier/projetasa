public class InterfaceConnecteur {

    boolean requis;
    java.lang.String nom;

    Role role;

    public InterfaceConnecteur(){};

    public boolean getRequis () {
        return this.requis;
    }
    public java.lang.String getNom () {
        return this.nom;
    }

    public void setRequis (boolean requis) {
        this.requis = requis;
    }
    public void setNom (java.lang.String nom) {
        this.nom = nom;
    }

    public Role getRole () {
        return this.role;
    }

    public void setRole (Role role) {
        this.role = role;
    }


}
