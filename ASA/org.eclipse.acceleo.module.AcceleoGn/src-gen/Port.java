public class Port {

    java.lang.String nom;

    Role role;

    public Port(){};

    public java.lang.String getNom () {
        return this.nom;
    }

    public void setNom (java.lang.String nom) {
        this.nom = nom;
    }

    public Role getRole () {
        return this.role;
    }

    public void setRole (Role role) {
        this.role = role;
    }


}
