public class Role {

    java.lang.String nom;

    Port port;

    public Role(){};

    public java.lang.String getNom () {
        return this.nom;
    }

    public void setNom (java.lang.String nom) {
        this.nom = nom;
    }

    public Port getPort () {
        return this.port;
    }

    public void setPort (Port port) {
        this.port = port;
    }


}
