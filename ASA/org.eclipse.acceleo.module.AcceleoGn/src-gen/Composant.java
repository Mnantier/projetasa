public class Composant {

    java.lang.String nom;

    System system;
    Interface interface;

    public Composant(){};

    public java.lang.String getNom () {
        return this.nom;
    }

    public void setNom (java.lang.String nom) {
        this.nom = nom;
    }

    public System getSystem () {
        return this.system;
    }
    public Interface getInterface () {
        return this.interface;
    }

    public void setSystem (System system) {
        this.system = system;
    }
    public void setInterface (Interface interface) {
        this.interface = interface;
    }


}
