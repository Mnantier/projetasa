public class InterfaceComposant {

    boolean requis;
    java.lang.String nom;

    Port port;
    Service service;

    public InterfaceComposant(){};

    public boolean getRequis () {
        return this.requis;
    }
    public java.lang.String getNom () {
        return this.nom;
    }

    public void setRequis (boolean requis) {
        this.requis = requis;
    }
    public void setNom (java.lang.String nom) {
        this.nom = nom;
    }

    public Port getPort () {
        return this.port;
    }
    public Service getService () {
        return this.service;
    }

    public void setPort (Port port) {
        this.port = port;
    }
    public void setService (Service service) {
        this.service = service;
    }


}
