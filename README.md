# ProjetASA

* **Maxence COUTAND**
* **Tom LE BERRE**

****************

* [Lien vers le rapport projet ASA](/documents/Projet_ASA.pdf)
* [Lien vers la génération de code avec Acceleo](/ASA/runtime-SiruisConfiguration/org.eclipse.acceleo.module.AcceleoGen/src/org/eclipse/acceleo/module/AcceleoGen/main/genJava.mtl)
* [Lien vers les classes Java généré](/ASA/runtime-SiruisConfiguration/org.eclipse.acceleo.module.AcceleoGen/src-gen)
* [Lien vers l'utilisation du code généré](/ASA/runtime-SiruisConfiguration/asa/src/main/java/fr/univnantes/asa/)

## Métamodèle - M2

![M2](/documents/images/M2.PNG)

## Modèle - M1

![M1](/documents/images/Sirius.PNG)